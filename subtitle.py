import subscene_api

# دریافت نام فیلم و زبان مورد نظر از کاربر
movie_name = input("نام فیلم را وارد کنید: ")
language = input("زبان مورد نظر را وارد کنید (مثلا en، fa و غیره): ")

# جستجوی زیرنویس با استفاده از کتابخانه subscene_api
subscene = subscene_api.Subscene(language)
subtitle_list = subscene.search_subtitles(movie_name)

# نمایش لیست زیرنویس‌های یافت شده برای انتخاب توسط کاربر
print("لیست زیرنویس‌های یافت شده:")
for index, subtitle in enumerate(subtitle_list):
    print(f"{index+1}. {subtitle.title} - {subtitle.uploader} - {subtitle.upload_date}")

# دریافت انتخاب کاربر و ایجاد لینک دانلود
selected_index = int(input("شماره زیرنویس مورد نظر خود را وارد کنید: ")) - 1
selected_subtitle = subtitle_list[selected_index]
download_link = subscene.download_subtitle(selected_subtitle)
print(f"لینک دانلود زیرنویس: {download_link}")

